#!/usr/bin/ruby

require 'minitest/autorun'
require_relative '../lib/debian_move_tags_files'

class DebianMoveTagsFilesTest < MiniTest::Test
  def test_files
    move = DebianMoveTagsFiles.new('data/debian-before/')
    assert_equal("libkf5attica-dev.install", move.dev_file)
    assert_equal("libkf5attica-doc.install", move.doc_file)
  end
end
