class DebianMoveTagsFiles
  attr_accessor :tags_line
  attr_accessor :doc_file
  attr_accessor :dev_file
    
  def initialize(debian_directory='.')
    files = Dir.entries(debian_directory)
    files.each do |file|
      if file.include?('-dev')
        @dev_file = file
      end
      if file.include?('-doc')
        @doc_file = file
      end
    end
  end

  def edit_doc()
  end
  
  def edit_dev()
  end

end
